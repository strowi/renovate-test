module.exports = {
  "hostRules": [
    {
      "matchHost": "registry.gitlab.com",
      "username": "xyz",
      "password": "xyz"
    }
  ],
   "registryAliases": {
    "gitlab.com/aedifion.io/dependency_proxy/containers": "docker.io"
  },

  "repositories": [
    "strowi/renovate-demo"
  ]
};
